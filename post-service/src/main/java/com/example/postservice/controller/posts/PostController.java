package com.example.postservice.controller.posts;

import com.example.postservice.controller.posts.requests.CreatePostRequest;
import com.example.postservice.controller.posts.requests.UpdatePostDetailsRequest;
import com.example.postservice.controller.posts.response.CreatePostResponse;
import com.example.postservice.model.posts.Post;
import com.example.postservice.service.posts.PostService;
import jakarta.persistence.PostUpdate;
import jakarta.ws.rs.PUT;
import lombok.AllArgsConstructor;
//import jakarta.validation.Valid;
import lombok.extern.java.Log;
import org.apache.logging.log4j.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/post")
@AllArgsConstructor
public class PostController {

    private final PostService postService;

    private static final Logger LOGGER = LoggerFactory.getLogger(PostController.class);

    @GetMapping("/getAll")
    public List<CreatePostResponse> getAllPosts() {
       return postService.getAllPosts();
    }

    @GetMapping("/getAll/{authorID}")
    public List<Post> getAllUserPosts(@PathVariable UUID authorID){
        List<Post> userPosts = postService.getAllPostWithAuthorID(authorID);
        if(!userPosts.isEmpty()){
            return userPosts;
        }
        return null;
    }
    @GetMapping("/{postID}")
    public Post getUserPost(@PathVariable UUID postID) {
    Post userPost = postService.findPostWithPostID(postID);
        if (userPost != null ){
            return userPost;
        }
        return null;
    }


    @PutMapping("/deactivateAllUserPosts/{authorId}")
    public void deactivateAllUserPosts(@PathVariable UUID authorId){

        //ResponseEntity response =
        postService.deactivateAllPostsWithAuthorID(authorId);

    }


    @PutMapping("/deactivatePost/{postID}")
//    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> setPostsInactive(@PathVariable UUID postID) {
        if(postService.findPostWithPostID(postID) != null){
            postService.deactivatePostWithPostID(postID);
            return ResponseEntity.ok("Post has been deactivated");
        }

            return ResponseEntity.notFound().build();
    }

    @DeleteMapping("/deletePostsByAuthorID/{authorID}")
    public ResponseEntity<String> deleteAllPostByAuthorID(@PathVariable UUID authorID) {
        ResponseEntity response =  postService.deleteAllPostsWithAuthorID(authorID);
        return ResponseEntity.ok(response.toString());

    }

    @DeleteMapping("/deletePost/{postID}")
//    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<String> deletePost(@PathVariable UUID postID) {
        if(postService.findPostWithPostID(postID) != null){
            postService.deletePostWithPostID(postID);
            return ResponseEntity.ok("Post has been deactivated");
        }

        return ResponseEntity.notFound().build();
    }

    @PutMapping("/updatePost")
    public ResponseEntity<String> updatePost(@RequestBody UpdatePostDetailsRequest post){

//        UpdatePostDetailsRequest request = postService.mapToUpdatePostDetailsRequest(post);
        postService.updatePost(post);

        return ResponseEntity.ok("It updated");

    }

    @PostMapping("/createPost")
    public ResponseEntity<CreatePostResponse> createPost(@RequestBody CreatePostRequest request){
//        Post response = postService.savePost(request);
        CreatePostResponse response1 = postService.createPost(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response1);
    }



}
