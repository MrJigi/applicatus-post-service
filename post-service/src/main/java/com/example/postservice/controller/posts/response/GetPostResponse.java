package com.example.postservice.controller.posts.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetPostResponse {
    private UUID userID;
    private UUID postID;
    private String topic;
    private String content;
    private LocalDateTime postedAt;
    private String userName;
    private String screenName;
    private Boolean isDeleted;
}
