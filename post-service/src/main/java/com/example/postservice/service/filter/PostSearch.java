package com.example.postservice.service.filter;

import com.example.postservice.model.posts.Post;

import java.util.ArrayList;
import java.util.List;

public class PostSearch  implements SearchStrategy<Post> {

    @Override
    public List<Post> search(List<Post> list, String keyword){
        List<Post> result = new ArrayList<>();
        keyword = keyword.toLowerCase();

        for (Post post : list) {
            String content = post.getContent().toLowerCase();
            if (content.contains(keyword)) {
                result.add(post);
            }
        }
        return result;
    }
}
